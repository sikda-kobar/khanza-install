#!/bin/bash
echo "-------------------------------------------"
echo "|   SSSSSS  II  KK   K  DDDDD    AAAAA    |"
echo "|   SS      II  KK K    DD   D  AA   AA   |"
echo "|   SSSSSS  II  KKK     DD   D  AAAAAAA   |"
echo "|       SS  II  KK K    DD   D  AA   AA   |"
echo "|   SSSSSS  II  KK   K  DDDDD   AA   AA   |"
echo "-------------------------------------------"
echo "    SISTEM INFORMASI KESEHATAN DAERAH      "
echo "    DINAS KESEHATAN KOTAWARINGIN BARAT     "
echo "-------------------------------------------"
echo "     Install dan Konfigurasi Otomatis      "
echo "               SIMRS-Khanza                "
echo "-------------------------------------------"
echo "    Pastikan anda terhubung ke Internet    "
echo "==========================================="
echo " Saat proses berjalan akan ada permintaan  "
echo " password beberapa kali, mohon untuk diisi."
echo " proses akan berjalan lama tergantung      "
echo " koneksi internet anda.                    "
echo "-------------------------------------------"
sudo apt update
sudo apt dist-upgrade -y
sudo apt install apache2 -y
sudo apt install mariadb-server mariadb-client -y
sudo apt install php libapache2-mod-php php-mysql -y
sudo apt remove --purge java* -y
sudo apt install java8* -y
cd /home/$USER/Downloads
sudo apt install java-common -y
wget debian.opennms.org/dists/opennms-22/main/binary-all/oracle-java8-installer_8u131-1~webupd8~2_all.deb
sudo dpkg -i oracle-java8-installer_8u131-1~webupd8~2_all.deb
sudo rm -rf oracle-java8-installer_8u131-1~webupd8~2_all.deb
sudo apt install subversion -y
sudo apt install git -y
cd /home/$USER/
mkdir SIMRS-Khanza
cd SIMRS-Khanza
mkdir server
mkdir klien
svn export --force https://github.com/mas-elkhanza/SIMRS-Khanza.git/trunk/dist
mv dist klien
svn export --force https://github.com/mas-elkhanza/SIMRS-Khanza.git/trunk/webapps
mv webapps server
svn export --force https://github.com/mas-elkhanza/SIMRS-Khanza.git/trunk/KhanzaAntrianLoket/dist/suara
cd klien
mkdir suara
cd ..
cd suara
mv *mp3 /home/$USER/SIMRS-Khanza/klien/suara/
cd ..
rmdir suara
svn export --force https://github.com/mas-elkhanza/SIMRS-Khanza.git/trunk/KhanzaAntrianLoket/dist/KhanzaAntrianLoket.jar
mv KhanzaAntrianLoket.jar klien/antrianloket.jar
svn export --force https://github.com/mas-elkhanza/SIMRS-Khanza.git/trunk/KhanzaAntrianPoli/dist/KhanzaAntrianPoli.jar
mv KhanzaAntrianPoli.jar klien/antrianpoli.jar
svn export --force https://github.com/mas-elkhanza/SIMRS-Khanza.git/trunk/KhanzaHMSAnjungan/dist/KhanzaHMSAnjungan.jar
mv KhanzaHMSAnjungan.jar klien/anjunganmandiri.jar
svn export --force https://github.com/mas-elkhanza/SIMRS-Khanza.git/trunk/KhanzaPengenkripsiTeks/dist/KhanzaPengenkripsiTeks.jar
mv KhanzaPengenkripsiTeks.jar klien/pengenkripsiteks.jar
curl -o sik.sql https://raw.githubusercontent.com/mas-elkhanza/SIMRS-Khanza/master/sik.sql
mv sik.sql server/sik.sql
cd /home/$USER/SIMRS-Khanza/klien/dist/setting/ #coba koreksi disini
rm database.xml
wget https://gitlab.com/sikda-kobar/khanza-install/raw/master/Database_SIMRS-Khanza.xml
mv Database_SIMRS-Khanza.xml database.xml
cd /home/$USER/SIMRS-Khanza/server/webapps/conf/
rm conf.php
wget https://gitlab.com/sikda-kobar/khanza-install/raw/master/Conf_SIMRS-Khanza.php
mv Conf_SIMRS-Khanza.php conf.php
sudo mysqladmin -u root create SIMRS-Khanza
cd /home/$USER/Downloads
wget https://gitlab.com/sikda-kobar/khanza-install/raw/master/Membuat_User_Database_untuk_SIMRS-Khanza.sql
echo "-------------------------------------------"
echo "     masukan password mysql anda dengan    "
echo "        password user linux mint anda      "
echo "-------------------------------------------"
sudo mysql -u root -p < /home/$USER/Downloads/Membuat_User_Database_untuk_SIMRS-Khanza.sql
echo "-------------------------------------------"
echo " selanjutnya password mysql : admin123     "
echo "-------------------------------------------"
sudo mysql -u admin -p SIMRS-Khanza < /home/$USER/SIMRS-Khanza/server/sik.sql
cd /home/$USER/Desktop
wget https://gitlab.com/sikda-kobar/khanza-install/raw/master/Jalan_pintas_SIMRS-Khanza_pada_Desktop.sh
mv Jalan_pintas_SIMRS-Khanza_pada_Desktop.sh SIMRS-Khanza.sh
chmod +x SIMRS-Khanza.sh
echo "-------------------------------------------"
echo "             Instalasi Selesai             "
echo "-------------------------------------------"
echo "   Buka Desktop Klik 2x SIMRS-Khanza.sh    "
echo "           Pilih Run in Terminal           "
echo "-------------------------------------------"
echo "-------------------------------------------"
echo "    Pada menu SIMRS-Khanza pilih log in    "
echo "       dengan user:spv password:server     "
echo "-------------------------------------------"

